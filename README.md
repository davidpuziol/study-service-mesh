# Study Service Mesh

Comparativo e estudo de várias ferramentas capaz de prover uma service mesh para os nossos microserviços.

## **Microservicos vs Mononito**

Para entender o motivo do uso de uma **rede de malha (service mesh)** devemos entender um pouco de microserviços. Para entendimento melhor sobre o assunto vale uma longa pesquisada na internet. Em resumo, podemos destrinchar uma aplicação enorme (Monolito) em pequenas partes, e cada parte com sua responsabilidade especifica e seu mundo separado. Isso trás muitas vantagens e algumas desvantagens.

Vantagens:

- Crie pequeno erre pequeno
- Deploy menores
- Desenvolvimento independente
- Escalibilidade independente, ou seja, se esta "função" e a mais utilizada do sistema não precisamos escalar tudo, somente isso.
- Utilização de diferentes linguagens e recursos que são melhores para aquele problema.
- Segurança independente

Desvantagens:

- Complexidade
- Governança (É aqui que o service mesh atua)
- Integração com aplicações monoliticas legadas
- Segurança, mais pontos de gerenciamento
- Latência

![Monolito vs Microservicos](./pics/microservicos.jpg)

Geralmente, as aplicações modernas são desmembradas em microserviços e a essência dos microsserviços está na comunicação serviço a serviço.

Desafio a resolver:

- Em microserviços temos diversos serviços espalhados
- Cada serviço de ter uma infinidade de máquinas sendo que elas podem serem criadas e destruídas o tempo todo. Como saber então se um serviço está disponível? Qual máquina (ip) e porta devo acessar para encontrar o meu serviço?
 
## Padrão orientado a código
As primeiras tentativas de resolver tais preocupações concentraram-se no desenvolvimento de bibliotecas que fornecerão esses recursos. O conceito é baseado na incorporação de bibliotecas ao código base do serviço:

![Orientado a código](./pics/code-oriented-frameworks.png)

- Orientado à linguagem: Precisamos de quase uma biblioteca para cada linguagem de programação.
- Propenso a erros (implementação).
- Difícil atualizar cada microsserviço quando o sistema cresce.
- Adicione desafios e deveres técnicos às equipes de desenvolvimento.
- Diferentes equipes na mesma organização podem ter diferentes implementações.
Cada equipe deve manter sua implementação.

## Padrão Sidecar
Esse padrão permite que os aplicativos sejam compostos por componentes e tecnologias heterogêneos. Esse padrão é chamado Sidecar porque se assemelha a um sidecar preso a uma motocicleta. No padrão, o sidecar é anexado a um aplicativo pai e fornece recursos de suporte para o aplicativo. Assim, todas as preocupações com microsserviços serão tratadas pelo sidecar de forma homogênea e injetadas dinamicamente pela plataforma ao implantar o aplicativo.

![sidecar](./pics/sidecar-oriented-pattern.png)

# O que temos?

![sidecar](./pics/microservices-iceberg.png)

## Como funciona um service mesh (rede em malha)

O service discovery nos ajuda a encontrar serviços para que, de forma dinâmica, possamos fazer requisiçoes para instâncias efêmeras. O service discovery nos proverá um repositório (Service Registry) atualizado com todos os serviços online disponíveis para que possamos fazer nossas requisições.

O service mesh é uma camada de infraestrutura dedicada, incorporada diretamente em uma aplicação, como um proxy.

**Não é necessária uma camada de service mesh para que a comunicação possa ser codificada em cada serviço, mas conforme a complexidade da comunicação aumenta, o uso de service mesh se torna cada vez mais indispensável.**

Sem um service mesh, é necessário codificar cada microsserviço com a lógica que rege a comunicação de serviço a serviço. Além disso, a falta da malha dificulta a identificação de falhas na comunicação, fora o custo que se tem em ter que implementar network load balancer internos.

Essa camada de infraestrutura visível pode documentar o desempenho das interações entre os diferentes componentes da aplicação, facilitando a otimização da comunicação e evitando o downtime, conforme a aplicação evolui, o que facilita bastante a observabilidade gráfica da rede inclusive.

Em um service mesh, as solicitações são encaminhadas entre microsserviços utilizando proxies em uma **camada de infraestrutura própria (infra virtual sobre infra)**. Por esse motivo, os proxies que compõem o service mesh são, às vezes, chamados de "sidecars", pois são executados paralelamente a cada serviço, não dentro dele. Juntos, esses proxies "sidecars", desacoplados de cada serviço, formam uma rede em malha.

![Service Mesh](./pics/servicemesh.jpg)

Mas, o que acontece se alguns serviços, como o banco de dados do inventário da loja, ficarem sobrecarregados? O service mesh resolve o problema, encaminhando as solicitações de um serviço para o próximo e otimizando como todos esses elementos funcionam juntos.

Um service mesh também captura todos os aspectos da comunicação de serviço a serviço como métricas de desempenho. Com o passar do tempo, é possível aplicar os dados fornecidos pela service mesh às regras de comunicação entre serviços para aumentar a eficiência e a confiabilidade das solicitações de serviço, d


vantagens:

- Os desenvolvedores podem focar em agregar mais valor aos negócios, em vez de se ocuparem com a conexão entre serviços.
- O rastreamento distribuído de solicitações por meio do Jaeger forma uma camada de infraestrutura visível paralela aos serviços. Dessa forma, é mais fácil reconhecer e diagnosticar os problemas.
- As aplicações ficam mais resilientes ao downtime, já que a service mesh pode rotear as solicitações para não passarem pelos serviços com falhas.
- As métricas de desempenho oferecem sugestões sobre como otimizar a comunicação no ambiente de execução.
- descoberta de serviço
- monitoramento de integridade do aplicativo
- balanceamento de carga
- failover automático
- criptografia
- observabilidade e rastreabilidade
- autenticação e autorização
- automação de rede
- aplicação de confiança zero



## Estado da Arte

Existem muitas ferramentas capaz de prover um service mesh, porém gostaria

## **[Istio](https://istio.io/)**

Malha de serviço líder hoje para implantações do Kubernetes, embora também ofereça suporte a VMs e endpoints fora do Kubernetes.

![Istio](./pics/istioarch.png)

Conexões, segurança, controle e observabilidade são os pontos mais fortes.

- Necessita uma plataforma kubernetes para configuração, mas pode gerenciar  vms e outros endpoints fora do kubernetes
- Critptografia TLS para as comunicações de serviço a serviço
- Regras de autenticação baseadas em identidade de usuários
- Possível fazer limite de taxas, cotas e controle de acesso ajudam a imperdir ataques de tráfego e acessos de usuários sem privilégios.
- Proxies envoy aumentam o desempenho e são fundamentais
- Gerenciamento de tráfego
- Autenticação via chave API Kubernetes e a segurança é gerenciada por uma conta de usuário privilegiada
- Suporte a vários clusters kubernetes simultâneos
- Manutenção (update e downgrade) através do control plane possibilitando ser feita através de um canary deploy
- CLI disponível com o istioctl
- Oferece muitas vantagens aos devotos do kubernetes
- Complexo assim como a arquitetura de microserviços na qual ele é executado
- Usa códigos de terceiro
- Custo de recursos um pouco elevado, pois é pouco leve

## **[Linkerd](https://linkerd.io/)**

Mais simples do que o Istio e construido principalmente para uma estrutura kubernetes.

![Linkerd](./pics/linkerdarch.png)

taxa, latencia, rastreamento
Balanceamento de carga
Plano e controle indepentende e de dados escrito em rust

- Necessita uma plataforma kubernetes para configuração, mas pode gerenciar  vms e outros endpoints fora do kubernetes
- Critptografia TLS para as comunicações de serviço a serviço com rotação de chave por padrão
- Poucas coisas podem interromper a manutenção administrativa pela simplicidade
- Visualização por invocação do cli para monitoramento em tempo real
- Agrupamento de funções na configuração padrão com inumeras bibliotecas de forma voluntária
- CLI disponível
- Não há limitação de taxa, injeção de atrasos ou interrupção de circuito
- Complexo assim como a arquitetura de microserviços na qual ele é executado
- Não expoe api do plano de conrole

## **[Consul](https://www.consul.io/)**

Mais antiga do que as demais com a proposta de conectar qualquer serviço em qualquer nuvem privada.

Descoberta via DNS ou HTTP
Interrupções de tráfego
Armazenamento hierarquico de chaves-valor
Integração nativa com o Vault

- Uso de um único binário que oferece todos os recursos necessário sem a utiliação de código de terceiros, não sendo necessário sistemas adicionais.
- Pode usar com máquinas virtuais (VMs), contêineres ou com plataformas de orquestração de contêineres, como Nomad e Kubernetes.
- Multiplataforma podendo ser usado no Kubernetes ou em outros ambientes como uma nuvem hibrida.
- Instalação pelo helm para um cluster k8s
- Não utiliza plataformas de terceiros
- Documentação rica
- Cli disponível para configurações mais avançadas ou UI permitindo visualizar seus serviços, nós, chaves, etc
- Critptografia TLS para as comunicações de serviço a serviço
- Fácil atualização
- Pode ser usado agentes nos nós ou proxie com envoy

### Outros
- [linkerd](https://linkerd.io/)
- [traefik-mesh](https://traefik.io/traefik-mesh/)
- [openservicemesh](https://openservicemesh.io/)
- [NSM](https://www.nginx.com/blog/introducing-nginx-service-mesh/)
- [Kuma](https://kuma.io/)
- [zookeper](https://zookeeper.apache.org/)

Qual escolher?

Depois de um belo estudo, acho que ferramenta ideial para a maioria dos cenários é o **Consul**.