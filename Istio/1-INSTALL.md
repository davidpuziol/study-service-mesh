
# Instalação do Istio

Vamos instalar o Istio de forma manual para aprendizado, mas pode ser feito depois via helm e terraforms, ansible, etc, de forma mais automática.

<https://istio.io/latest/docs/setup/getting-started/>

````bash
curl -L https://istio.io/downloadIstio | sh -
sudo cp istio-*/bin/istioctl /usr/local/bin
````

A instalação do istio é baseada em alguns [profiles](https://istio.io/latest/docs/setup/additional-setup/config-profiles/) que já vem com alguns recursos instalados.

- default: instalação padrão (mais usada até para produção)
- demo: é mais para mostrar as funcionalidade e ja vem mais elaborado e ja vem pronto para rodar a bookinfo para entender como a coisa funciona.
- minimal: menor instalação possível para depois vc colocar o que vc precisa.
- external: instalação para um cluster remoto gerenciado por um control plane em um cluster primário
- preview: instala novas features que estão em testes
- empty: não deploya nada, usado para configuração customizada

![profiles](../pics/install-istio-profiles.jpg)



````bash
❯ istioctl install --set profile=demo -y
✔ Istio core installed        
✔ Istiod installed       
✔ Egress gateways installed
✔ Ingress gateways installed           
✔ Installation complete
Making this installation the default for injection and validation.
````

O Istio cuida de namespaces específicas de acordo com o que é apontado para ele. Adicione um rótulo de namespace para instruir o Istio a injetar automaticamente proxies sidecar do Envoy quando você implantar seu aplicativo posteriormente:

````bash
kubectl label namespace default istio-injection=enabled
````

**obs: é possível injetar o envoy somente em um deployment caso não queira utilizar todo um namespace com o comando  istioctl kube-inject**

Vamos fazer um describe para ver o rotulo sendo colocado

````bash
kubectl describe namespace default                           
Name:         default
            # veja aqui
Labels:       istio-injection=enabled
              kubernetes.io/metadata.name=default
Annotations:  <none>
Status:       Active
No resource quota.
No LimitRange resource.
````

A instalaçao do istio cria um namespace para colocar os pods dele. Vamos conferir.

````bash
~/projects/study-service-mesh main ⇡1 !2 ?9                                                                           ⎈ kind-kind  1.1.7 14:09:17
❯ k get ns                  
NAME                 STATUS   AGE
default              Active   61m
# veja abaixo
istio-system         Active   7m12s
kube-node-lease      Active   61m
kube-public          Active   61m
kube-system          Active   61m
local-path-storage   Active   61m

~/projects/study-service-mesh main ⇡1 !2 ?9                                                                           ⎈ kind-kind  1.1.7 14:10:03
❯ k get pods -n istio-system
NAME                                    READY   STATUS    RESTARTS   AGE
istio-egressgateway-5d67ff499d-7rwqh    1/1     Running   0          7m1s
istio-ingressgateway-579c849b8d-ggsdz   1/1     Running   0          7m1s
istiod-8cbfb5596-fzk99                  1/1     Running   0          7m17s
````

## Instalação com o Helm

Adicionando o repositório do Helm para o Istio. Para este cenário não existe profile.

````bash
helm repo add istio https://istio-release.storage.googleapis.com/charts
helm repo update
````

Criando o namespace dedicado ao istio

````bash
kubectl create namespace istio-system
kubectl label namespace default istio-injection=enabled
````

````bash
helm install istio-base istio/base -n istio-system
helm install istiod istio/istiod -n istio-system --wait
````

(Opcional) Instale um gateway de entrada:

````bash
kubectl create namespace istio-ingress
kubectl label namespace istio-ingress istio-injection=enabled
helm install istio-ingress istio/gateway -n istio-ingress --wait
````

helm status istiod -n istio-system

## Istioctl auto complete

Para o auto complete do istioctl no zsh

````bash
mkdir ~/.completions
istioctl completion zsh > ~/.completions/_istioctl
echo "source ~/.completions/_istioctl" >> ~/.zshrc
````

## Tetrate Istio Distro

Usando o [Tetrate Istio Distro](https://istio.tetratelabs.io/) , você pode passar o nome do perfil de configuração de instalação para instalar o Istio. Por exemplo, para instalar o perfil de demonstração, você pode executar este comando, mas precisa ter a cli [getmesh](https://istio.tetratelabs.io/getmesh-cli/).

````bash
getmesh istioctl install --set profile=demo
````

Além disso, você pode personalizar sua instalação do Istio, independentemente do perfil, passando --set <key>=<value>pares de chave/valor adicionais para o comando.