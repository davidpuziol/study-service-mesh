# Istio

![Istio](../pics/istiologo.png)

<https://istio.io/>

**Sobre este estudo, vale lembrar que é um estudo inicial das principais partes do Istio, mas que devemos sempre comer a documentação oficial e sempre consultá-la.**

Os [pilares do Istio](https://istio.io/latest/docs/concepts/) como service mesh são:

[Gestão de tráfego](https://istio.io/latest/docs/concepts/traffic-management/) 

- Deploy canario
- Balancemaneto de carga
- Healthchecks
- Forçar erros para ver como a aplicação se comporta

[Segurança](https://istio.io/latest/docs/concepts/security/)

- Mutual tls (service to service security)
- Criptografia
- Auditoria

[Observabilidade](https://istio.io/latest/docs/concepts/traffic-management/)

- Ambiente gráfico para análise das comunições
- Métricas
- Traces
- Logs

[Extensibilidade](https://istio.io/latest/docs/concepts/wasm/)

Os plugins são desenvolvidor com base nesses pré requisitos:

- Eficiência
- Função
- Isolamento
- Configuração
- Operador
- Desenvolvedor de extensão

É o mais popular service mesh do mercado. Precisa ser deployado em um cluster kubernestes. É um projeto totalmente open source <https://github.com/istio/istio>.
Para utilizar o Istio **é necessário um estudo aprofundado, pois ele pode performar mal a aplicaçao se não for bem configurado**, por isso é necessário usar e testar com cautela e criar labs para análise antes de ir para o ambiente de produção.

## Arquitetura

[Documentacao Oficial](https://istio.io/latest/docs/)

![Istio Arch](../pics/istioarch.jpg)

### **Envoys**

O Istio usa uma versão estendida do Envoy (proxy) como plano de dados. O data plane é o conjunto de envoys que forma a rede mesh do istio. O envoy escuta tudo o que esta aconcendo de comucaniação com aquele host e reporta essa comunicação par ao control plane. Ele trabalha como um sidecar na aplicação e mantém o control plane informado sobre todo o tipo de dados, desde o endpoint até as métricas. A comunicação entre os serviços acontece de proxy para proxy, o control plane somente servirá para consulta. É possível nos envoys controlar como as requisições de entrada (ingress) e saídas (egress).

### **Control Plane**

É o cerebro, a cabeça do Istio, que contem os dados entregues pelos envoys. Quando uma aplicação precisa consultar outra primeiro ele vai ao control plane para descobrir o endpoint. Mas além de ser um internal dns server, o control plane tem muitas outras funcões como controlar o tráfego, a segurança,a observabilidade, permissões, etc. O control plane consegue mandar os dados para o grafana para fornecer todo um plano de observabilidade, como o tracing, latência, etc.

- Pilot
- citadel
- Galley

## Requisitos mínimos

<https://istio.io/latest/docs/ops/deployment/requirements/>

1 - O envoy utiliza o user id account 1337, logo nenhuma aplicação deve fazer uso desse valor de uid.

2 - Verificar se NET_ADMIN e NET_RAW estão disponíveis para os pods do namespace que o istio controla.

````bash
for psp in $(kubectl get psp -o jsonpath="{range .items[*]}{@.metadata.name}{'\n'}{end}"); do if [ $(kubectl auth can-i use psp/$psp --as=system:serviceaccount:<your namespace>:<your service account>) = yes ]; then kubectl get psp/$psp --no-headers -o=custom-columns=NAME:.metadata.name,CAPS:.spec.allowedCapabilities; fi; done

for psp in $(kubectl get psp -o jsonpath="{range .items[*]}{@.metadata.name}{'\n'}{end}"); do if [ $(kubectl auth can-i use psp/$psp --as=system:serviceaccount:default:default) = yes ]; then kubectl get psp/$psp --no-headers -o=custom-columns=NAME:.metadata.name,CAPS:.spec.allowedCapabilities; fi; done
````

3 - Pods com labels de app e version adicionam informações contextuais às métricas e à telemetria que o Istio coleta.

4 - As portas e protocolos a seguir são usados ​​pelo proxy sidecar e pelo istiod. Evite usar as portas 443, 8080 e o range 15000 até 15100

**As portas e protocolos usados pelo plano de dados (envoy)**

| Porta | Protocolo | Descrição | Somente interno do pod |
|:---:|:---:|:---:|:---:|
| 15000 | TCP | Porta de administração do Envoy (comandos/diagnóstico) | Sim |
| 15001 | TCP | Enviado de saída | No |
| 15004 | HTTP | Porta de depuração | Sim |
| 15006 | TCP | Enviado de entrada | No |
| 15008 | H2 | Porta de túnel HBONE mTLS | No |
| 15009 | H2C | Porta HBONE para redes seguras | No |
| 15020 | HTTP | Telemetria do Prometheus mesclada do agente Istio, Envoy e aplicativo | No |
| 15021 | HTTP | Verificações de integridade | No |
| 15053 | DNS | Porta DNS, se a captura estiver habilitada | Sim |
| 15090 | HTTP | Telemetria Envoy Prometheus | No |

**As portas e protocolos usados pelo plano de controle istiod**

| Porta | Protocolo | Descrição | Somente host local |
|:---:|:---:|:---:|:---:|
| 443 | HTTPS | Porta de serviço de webhooks | No |
| 8080 | HTTP | Interface de depuração (obsoleta, apenas porta de contêiner) | No |
| 15010 | GRPC | Serviços XDS e CA (texto simples, apenas para redes seguras) | No |
| 15012 | GRPC | Serviços XDS e CA (TLS e mTLS, recomendados para uso em produção) | No |
| 15014 | HTTP | Monitoramento do plano de controle | No |
| 15017 | HTTPS | Porta de contêiner do Webhook, encaminhada de 443 | No |
| 15020 | HTTP | Telemetria do Prometheus mesclada do agente Istio, Envoy e aplicativo | No |
| 15021 | HTTP | Verificações de integridade | No |
| 15053 | DNS | Porta DNS, se a captura estiver habilitada | Sim |
| 15090 | HTTP | Telemetria Envoy Prometheus | No |

Como falado anteriormente, o Istio deve ser deployado em um cluster k8s. Crie um cluster k8s para começar a brincadeira. Nessa instalação foi usado o [kind](https://kind.sigs.k8s.io/docs/user/quick-start/#installation) com 3 nodes um master e dois workers.
Faça a instalaçao do [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/) e vamos a criação do cluster

````bash
curl -Lo ./kind https://kind.sigs.k8s.io/dl/v0.14.0/kind-linux-amd64
chmod +x ./kind 
sudo mv kind /usr/local/bin
mkdir ~/.kind
echo "kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
nodes:
- role: control-plane
- role: worker
- role: worker" > ~/.kind/cluster.yaml
kind create cluster --config ~/.kind/cluster.yaml
kubectl config get-clusters 
````

# Workshop 
<https://www.istioworkshop.io/>
