## Deploy primeira aplicaçao exemplo

Como exemplo vamos implantar o exemplo dentro da pasta samples chamado bookinfo. Seguindo esta documentação <https://istio.io/latest/docs/setup/getting-started/>

````bash
kubectl apply -f istio-*/samples/bookinfo/platform/kube/bookinfo.yaml 
service/details created
serviceaccount/bookinfo-details created
deployment.apps/details-v1 created
service/ratings created
serviceaccount/bookinfo-ratings created
deployment.apps/ratings-v1 created
service/reviews created
serviceaccount/bookinfo-reviews created
deployment.apps/reviews-v1 created
deployment.apps/reviews-v2 created
deployment.apps/reviews-v3 created
service/productpage created
serviceaccount/bookinfo-productpage created
deployment.apps/productpage-v1 created
````

Observe na aplicação exemplo ele tem 3 versões do reviews rodando e em cada uma delas tem 2 containers dentro do pod... 2/2. Isso significa que tem o container rodando e o envoy.

````bash
k get deploy                                                  
NAME             READY   UP-TO-DATE   AVAILABLE   AGE
details-v1       1/1     1            1           2m25s
productpage-v1   1/1     1            1           2m25s
ratings-v1       1/1     1            1           2m25s
reviews-v1       1/1     1            1           2m25s
reviews-v2       1/1     1            1           2m25s
reviews-v3       1/1     1            1           2m25s

❯ k get pods
NAME                              READY   STATUS    RESTARTS   AGE
details-v1-b48c969c5-jfbft        2/2     Running   0          2m2s
productpage-v1-74fdfbd7c7-fcbw5   2/2     Running   0          2m2s
ratings-v1-b74b895c5-kbdj5        2/2     Running   0          2m2s
reviews-v1-68b4dcbdb9-wv2ss       2/2     Running   0          2m2s
reviews-v2-565bcd7987-6s47t       2/2     Running   0          2m2s
reviews-v3-d88774f9c-qpnpk        2/2     Running   0          2m2s
````

Vamos fazer o describe do pod para conferir o envoy

````
k describe pods reviews-v1-68b4dcbdb9-wv2ss 
Name:         reviews-v1-68b4dcbdb9-wv2ss
Namespace:    default
Priority:     0
Node:         kind-worker2/172.25.0.3
Start Time:   Tue, 07 Jun 2022 14:21:20 -0300
Labels:       app=reviews
              pod-template-hash=68b4dcbdb9
              security.istio.io/tlsMode=istio
              service.istio.io/canonical-name=reviews
              service.istio.io/canonical-revision=v1
              version=v1
Annotations:  kubectl.kubernetes.io/default-container: reviews
              kubectl.kubernetes.io/default-logs-container: reviews
              prometheus.io/path: /stats/prometheus
              prometheus.io/port: 15020
              prometheus.io/scrape: true
              sidecar.istio.io/status:
                {"initContainers":["istio-init"],"containers":["istio-proxy"],"volumes":["workload-socket","workload-certs","istio-envoy","istio-data","is...
Status:       Running
IP:           10.244.1.5
IPs:
  IP:           10.244.1.5
Controlled By:  ReplicaSet/reviews-v1-68b4dcbdb9
Init Containers:
  istio-init:
    Container ID:  containerd://61ccd1ee70e482722fd6dcb8cdb77f731c38feea9904fea6e2f5892e7a600c2d
    Image:         docker.io/istio/proxyv2:1.14.0
    Image ID:      docker.io/istio/proxyv2@sha256:0097754ec224e387ecf0ce843a12761c3922f4a0f83ab43d119629e2fce88c8b
    Port:          <none>
    Host Port:     <none>
    Args:
      istio-iptables
      -p
      15001
      -z
      15006
      -u
      1337
      -m
      REDIRECT
      -i
      *
      -x
      
      -b
      *
      -d
      15090,15021,15020
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Tue, 07 Jun 2022 14:21:21 -0300
      Finished:     Tue, 07 Jun 2022 14:21:21 -0300
    Ready:          True
    Restart Count:  0
    Limits:
      cpu:     2
      memory:  1Gi
    Requests:
      cpu:        10m
      memory:     40Mi
    Environment:  <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from kube-api-access-vw496 (ro)
Containers:
  reviews:
    Container ID:   containerd://582a6725ed7845f0b719904d8edd0bee57236810bf070ca12edb232952446381
    Image:          docker.io/istio/examples-bookinfo-reviews-v1:1.16.4
    Image ID:       docker.io/istio/examples-bookinfo-reviews-v1@sha256:fbc61607e475721fb2459b72e0dca1469b958a85106bd70d864e0e1d2834a921
    Port:           9080/TCP
    Host Port:      0/TCP
    State:          Running
      Started:      Tue, 07 Jun 2022 14:23:06 -0300
    Ready:          True
    Restart Count:  0
    Environment:
      LOG_DIR:  /tmp/logs
    Mounts:
      /opt/ibm/wlp/output from wlp-output (rw)
      /tmp from tmp (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from kube-api-access-vw496 (ro)
# veja o PROXY AQUI
  istio-proxy:
    Container ID:  containerd://d4c7ad96166ad2b11810f880e41997227147098b924142a8efb231cfbe07417c
    Image:         docker.io/istio/proxyv2:1.14.0
    Image ID:      docker.io/istio/proxyv2@sha256:0097754ec224e387ecf0ce843a12761c3922f4a0f83ab43d119629e2fce88c8b
    Port:          15090/TCP
    Host Port:     0/TCP
    Args:
      proxy
      sidecar
      --domain
      $(POD_NAMESPACE).svc.cluster.local
      --proxyLogLevel=warning
      --proxyComponentLogLevel=misc:error
      --log_output_level=default:info
      --concurrency
      2
    State:          Running
      Started:      Tue, 07 Jun 2022 14:23:07 -0300
    Ready:          True
    Restart Count:  0
    Limits:
      cpu:     2
      memory:  1Gi
    Requests:
      cpu:      10m
      memory:   40Mi
    Readiness:  http-get http://:15021/healthz/ready delay=1s timeout=3s period=2s #success=1 #failure=30
    Environment:
      JWT_POLICY:                    third-party-jwt
      PILOT_CERT_PROVIDER:           istiod
      CA_ADDR:                       istiod.istio-system.svc:15012
      POD_NAME:                      reviews-v1-68b4dcbdb9-wv2ss (v1:metadata.name)
      POD_NAMESPACE:                 default (v1:metadata.namespace)
      INSTANCE_IP:                    (v1:status.podIP)
      SERVICE_ACCOUNT:                (v1:spec.serviceAccountName)
      HOST_IP:                        (v1:status.hostIP)
      PROXY_CONFIG:                  {}
                                     
      ISTIO_META_POD_PORTS:          [
                                         {"containerPort":9080,"protocol":"TCP"}
                                     ]
      ISTIO_META_APP_CONTAINERS:     reviews
      ISTIO_META_CLUSTER_ID:         Kubernetes
      ISTIO_META_INTERCEPTION_MODE:  REDIRECT
      ISTIO_META_WORKLOAD_NAME:      reviews-v1
      ISTIO_META_OWNER:              kubernetes://apis/apps/v1/namespaces/default/deployments/reviews-v1
      ISTIO_META_MESH_ID:            cluster.local
      TRUST_DOMAIN:                  cluster.local
    Mounts:
      /etc/istio/pod from istio-podinfo (rw)
      /etc/istio/proxy from istio-envoy (rw)
      /var/lib/istio/data from istio-data (rw)
      /var/run/secrets/istio from istiod-ca-cert (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from kube-api-access-vw496 (ro)
      /var/run/secrets/tokens from istio-token (rw)
      /var/run/secrets/workload-spiffe-credentials from workload-certs (rw)
      /var/run/secrets/workload-spiffe-uds from workload-socket (rw)
Conditions:
  Type              Status
  Initialized       True 
  Ready             True 
  ContainersReady   True 
  PodScheduled      True 
Volumes:
  workload-socket:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  workload-certs:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  istio-envoy:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     Memory
    SizeLimit:  <unset>
  istio-data:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  istio-podinfo:
    Type:  DownwardAPI (a volume populated by information about the pod)
    Items:
      metadata.labels -> labels
      metadata.annotations -> annotations
  istio-token:
    Type:                    Projected (a volume that contains injected data from multiple sources)
    TokenExpirationSeconds:  43200
  istiod-ca-cert:
    Type:      ConfigMap (a volume populated by a ConfigMap)
    Name:      istio-ca-root-cert
    Optional:  false
  wlp-output:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  tmp:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:     
    SizeLimit:  <unset>
  kube-api-access-vw496:
    Type:                    Projected (a volume that contains injected data from multiple sources)
    TokenExpirationSeconds:  3607
    ConfigMapName:           kube-root-ca.crt
    ConfigMapOptional:       <nil>
    DownwardAPI:             true
QoS Class:                   Burstable
Node-Selectors:              <none>
Tolerations:                 node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                             node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type    Reason     Age    From               Message
  ----    ------     ----   ----               -------
  Normal  Scheduled  4m49s  default-scheduler  Successfully assigned default/reviews-v1-68b4dcbdb9-wv2ss to kind-worker2
  Normal  Pulled     4m48s  kubelet            Container image "docker.io/istio/proxyv2:1.14.0" already present on machine
  Normal  Created    4m48s  kubelet            Created container istio-init
  Normal  Started    4m48s  kubelet            Started container istio-init
  Normal  Pulling    4m47s  kubelet            Pulling image "docker.io/istio/examples-bookinfo-reviews-v1:1.16.4"
  Normal  Pulled     3m3s   kubelet            Successfully pulled image "docker.io/istio/examples-bookinfo-reviews-v1:1.16.4" in 1m44.440618851s
  Normal  Created    3m3s   kubelet            Created container reviews
  Normal  Started    3m3s   kubelet            Started container reviews
  Normal  Pulled     3m3s   kubelet            Container image "docker.io/istio/proxyv2:1.14.0" already present on machine
  Normal  Created    3m3s   kubelet            Created container istio-proxy
  Normal  Started    3m2s   kubelet            Started container istio-proxy
````

O comando abaixo executa o comando kubectl de dentro do pod rattings e la dentro executa um curl no productpage e depois faz um grep para mostrar o title

````bash
kubectl exec "$(kubectl get pod -l app=ratings -o jsonpath='{.items[0].metadata.name}')" -c ratings -- curl -sS productpage:9080/productpage | grep -o "<title>.*</title>"

<title>Simple Bookstore App</title>
````

Quando instalamos o istio ele cria apis para criamos alguns resocures extensiveis no k8s, como por exemplo gateway e virtualservice

Vamos executar agora o gateway e o virtualservice para abrir a aplicação para acesso externo.

````yaml
❯ cat ./istio-1.14.0/samples/bookinfo/networking/bookinfo-gateway.yaml
apiVersion: networking.istio.io/v1alpha3
kind: Gateway
metadata:
  name: bookinfo-gateway
spec:
  selector:
    istio: ingressgateway # use istio default controller
  servers:
  - port:
      number: 80
      name: http
      protocol: HTTP
    hosts:
    - "*"
---
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: bookinfo
spec:
  hosts:
  - "*"
  gateways:
  - bookinfo-gateway
  http:
  - match:
    - uri:
        exact: /productpage
    - uri:
        prefix: /static
    - uri:
        exact: /login
    - uri:
        exact: /logout
    - uri:
        prefix: /api/v1/products
    route:
    - destination:
        host: productpage
        port:
          number: 9080
````

## O que é o gateway?

É um gateway de entrada para a nossa aplicação atravez do ip do istio control plane

## O que é um virtualservice?

É a definição do nosso serviço para o istio e deve ser vinculado ao gateway

````bash
kubectl apply -f istio-*/samples/bookinfo/networking/bookinfo-gateway.yaml
gateway.networking.istio.io/bookinfo-gateway created
virtualservice.networking.istio.io/bookinfo created
````

Para fazer uma validação se tudo esta ok o istio tem um comando analizy

````bash
istioctl analyze
✔ No validation issues found when analyzing namespace: default.
````

````bash
❯ k get virtualservices                                                     
NAME       GATEWAYS               HOSTS   AGE
bookinfo   ["bookinfo-gateway"]   ["*"]   170m

❯ k get gateway        
NAME               AGE
bookinfo-gateway   170m
````

agora vamos ter que acessar atravez do ip privado, pois não temos um load balancer, afinal estamos usando o kind para teste

````bash
❯ k get svc istio-ingressgateway -n istio-system 
NAME                   TYPE           CLUSTER-IP     EXTERNAL-IP   PORT(S)                                                                      AGE
istio-ingressgateway   LoadBalancer   10.96.149.13   <pending>     15021:32076/TCP,80:31498/TCP,443:31818/TCP,31400:32020/TCP,15443:30134/TCP   8h
````

Com esses comandos abaixo somente filtrou o service referente istio-ingressgateway pegando as portas http2 e https e colocou em uma variavel e montou o url do istio para nós

````bash
export INGRESS_PORT=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.spec.ports[?(@.name=="http2")].nodePort}')
export SECURE_INGRESS_PORT=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.spec.ports[?(@.name=="https")].nodePort}')
export INGRESS_HOST=$(kubectl get po -l istio=ingressgateway -n istio-system -o jsonpath='{.items[0].status.hostIP}')
export GATEWAY_URL=$INGRESS_HOST:$INGRESS_PORT
❯ echo "$GATEWAY_URL"
172.25.0.3:31498
````

Se acessarmos essa url no navegador http://172.25.0.2:31498/productpage ou /login ou /logout ou qualquer outro match que foi definido no virtual server, bateremos nos servicos v1 v2 e v3, pois todos respondem na porta 9080 que esta definido no sample que levantamos.

# BookInfo Overview

![bookinfopage](../pics/bookinfodesign.jpg)

Este exemplo implanta um aplicativo de amostra composto por quatro microsserviços separados usados ​​para demonstrar vários recursos do Istio.

O aplicativo Bookinfo é dividido em quatro microsserviços separados:

O  microsserviço **productpage** chama os microsserviços details e reviews para preencher a página. Escrito em **python**

O microserviço **details** contém informações do livro. Escrito em **Ruby**.
reviews.

 O microserviço **reviews** contém resenhas de livros. Ele também chama o ratings microsserviço. Escrito em Java.

O microserviço ratings contém informações de classificação de livros que acompanham uma resenha de livro.

Existem 3 versões do microserviço de review:

A versão v1 não chama o ratingsserviço.
A versão v2 chama o ratingsserviço e exibe cada classificação como 1 a 5 estrelas pretas.
A versão v3 chama o ratingsserviço e exibe cada classificação como 1 a 5 estrelas vermelhas.
A arquitetura de ponta a ponta do aplicativo é mostrada abaixo.

![bookinfopage](../pics/bookinfopage.jpg)

Uma visão no kiali das requisições

![bookinfopage](../pics/bookinfo.jpg)
