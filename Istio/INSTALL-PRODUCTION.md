
# Instalação do Istio em produção com helm

<https://istio.io/latest/docs/setup/install/helm/>

Para instalação em produção sempre vamos instalar o mínimo necessário e ir adicionando as coisas aos poucos.
A instalação do grafana, kiali, prometheus, jaeger, etc, vamos utilizar o helm de cada um deles.

O mínimo necessário será a instalação profile default que instala somente o istiod e um ingress gateway

<https://preliminary.istio.io/latest/docs/reference/config/>
<https://istio.io/latest/docs/ops/integrations/>
## Instalação com o helm

Precisar olhar o pre requisitos antes
<https://istio.io/latest/docs/setup/platform-setup/prerequisites/> e ter o helm instalado
Lembrando que o helm deve ser instalado com um tiller pré configurado

````bash
#download do istio
curl -L https://istio.io/downloadIstio | sh -
sudo cp istio-*/bin/istioctl /usr/local

#instalacao do helm
curl https://raw.githubusercontent.com/kubernetes/helm/master/scripts/get > get_helm.sh
chmod +x get_helm.sh
./get_helm.sh
helm version
#cria uma service account para o helm

helm repo add istio https://istio-release.storage.googleapis.com/charts
helm repo update
kubectl create namespace istio-system

helm install istio-base istio/base -n istio-system
helm install istiod istio/istiod -n istio-system --wait

kubectl create namespace istio-ingress
kubectl label namespace istio-ingress istio-injection=enabled
helm install istio-ingress istio/gateway -n istio-ingress --wait

kubectl create namespace istio-egress
kubectl label namespace istio-egress istio-injection=enabled
helm install istio-egress istio/gateway -n istio-egress --wait

# INSTALANDO OS OPERATIONS - NAO SEI SE VALE A PENA INSTALAR PELO ISTIO OU SE É MELHOR IR DIRETO NO HELM
kubectl apply -f https://raw.githubusercontent.com/istio/istio/release-1.14/samples/addons/kiali.yaml
kubectl apply -f https://raw.githubusercontent.com/istio/istio/release-1.14/samples/addons/prometheus.yaml
kubectl apply -f https://raw.githubusercontent.com/istio/istio/release-1.14/samples/addons/grafana.yaml
kubectl apply -f https://raw.githubusercontent.com/istio/istio/release-1.14/samples/addons/jaeger.yaml

# Ativando o mutual tls

kubectl apply -f - <<EOF
apiVersion: security.istio.io/v1beta1
kind: PeerAuthentication
metadata:
  name: "default"
  namespace: "istio-system"
spec:
  mtls:
    mode: STRICT
EOF
````