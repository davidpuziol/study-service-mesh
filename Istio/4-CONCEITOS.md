# Debug geral

Quando instalamos o istio, automacamente criamos alguns deploys (que criaram seus replicas sets e seus pods) e alguns services.

````bash
❯ k get all -n istio-system                             
NAME                                        READY   STATUS    RESTARTS      AGE
pod/istio-egressgateway-5d67ff499d-7rwqh    1/1     Running   4 (24h ago)   2d1h
pod/istio-ingressgateway-579c849b8d-ggsdz   1/1     Running   4 (24h ago)   2d1h
pod/istiod-8cbfb5596-fzk99                  1/1     Running   6 (24h ago)   2d1h

NAME                           TYPE           CLUSTER-IP     EXTERNAL-IP   PORT
service/istio-egressgateway    ClusterIP      10.96.10.173   <none>        80/TCP,443/TCP                                                               2d1h
service/istio-ingressgateway   LoadBalancer   10.96.149.13   <pending>     15021:32076/TCP,80:31498/TCP,443:31818/TCP,31400:32020/TCP,15443:30134/TCP   2d1h
service/istiod                 ClusterIP      10.96.230.2    <none>        15010/TCP,15012/TCP,443/TCP,15014/TCP                                        2d1h

NAME                                   READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/istio-egressgateway    1/1     1            1           2d1h
deployment.apps/istio-ingressgateway   1/1     1            1           2d1h
deployment.apps/istiod                 1/1     1            1           2d1h

NAME                                              DESIRED   CURRENT   READY   AGE
replicaset.apps/istio-egressgateway-5d67ff499d    1         1         1       2d1h
replicaset.apps/istio-ingressgateway-579c849b8d   1         1         1       2d1h
replicaset.apps/istiod-8cbfb5596                  1         1         1       2d1h
````

Um ingress gateway pode ser escalado se alteramos o seu replicaset e inclusive colocar um hpa, o que é uma boa prática em ambientes produtivos.

## Preciso de vários Ingress Gateways

Antes de criar vários gateways de entrada (e vários balanceadores de carga com seu provedor de nuvem), verifique se você precisa disso. Os balanceadores de carga custam dinheiro e é outra coisa que você precisa gerenciar. Um único load balancer pode funcionar bem com muitos cenários, mas há casos em que você pode ter um load balancer privado ou interno e um segundo público.

O cenário com um único balanceador de carga seria semelhante à figura abaixo.

![Balanceador de carga único](../pics/single-load-balancer.png)

Temos um único gateway de entrada - um serviço Kubernetes com o tipo LoadBalancer e um Pod executando o Envoy. O fato de o serviço ser do tipo LoadBalancer causa a criação de uma instância real do balanceador de carga e nos fornece um endereço IP externo.

Com o recurso Istio Gateway, a chave do host na configuração e anexando um Gateway a um VirtualService, podemos expor vários serviços diferentes do cluster em diferentes nomes de domínio ou subdomínios.

Agora considere um cenário diferente em que você deseja que duas instâncias de balanceador de carga separadas sejam executadas - mostradas na figura abaixo.

![Dois balanceadores de carga](../pics/two-load-balancers.png)

As caixinhas representam gateways. 

Ingress Gateways e somente Gateways são coisas diferentes? Sim. Ingress gateway é o controler do gateways.
