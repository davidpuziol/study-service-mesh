# Gateway

https://istio.io/latest/docs/reference/config/networking/gateway/>

Vamos dar um describe no gateway que temos para analisar a saída e entender melhor o que é um gateway. Quando foi criada a aplicação exemplo criamos um gateway, então vamos vê-lo:

````bash
kubectl get gateways -o yaml
````

````yaml
apiVersion: v1
items:
- apiVersion: networking.istio.io/v1beta1
  kind: Gateway
  metadata:
    annotations:
      kubectl.kubernetes.io/last-applied-configuration: |
        {"apiVersion":"networking.istio.io/v1alpha3","kind":"Gateway","metadata":{"annotations":{},"name":"bookinfo-gateway","namespace":"default"},"spec":{"selector":{"istio":"ingressgateway"},"servers":[{"hosts":["*"],"port":{"name":"http","number":80,"protocol":"HTTP"}}]}}
    creationTimestamp: "2022-06-07T22:41:32Z"
    generation: 1
    name: bookinfo-gateway
    namespace: default
    resourceVersion: "38744"
    uid: 6ff5b5bf-6cda-402e-b939-7e2cdd562deb
  spec:
    selector:
      istio: ingressgateway
    servers:
    - hosts:
      - '*'
      port:
        name: http
        number: 80
        protocol: HTTP
kind: List
metadata:
  resourceVersion: ""
  selfLink: ""
````

Gateway descreve (configura) um balanceador de carga operando na borda da malha recebendo conexões HTTP/TCP de entrada ou saída. A especificação descreve um conjunto de portas que devem ser expostas, o tipo de protocolo a ser usado, a configuração SNI para o balanceador de carga, etc.

a linha

````yaml
    selector:
      istio: ingressgateway
````

Me diz que será aplicada a nossa configuração em cima de quem tem esse rótulo.
E quem tem esse rótulo?

````base
❯ k get pods -l istio=ingressgateway --all-namespaces
NAMESPACE      NAME                                    READY   STATUS    RESTARTS      AGE
istio-system   istio-ingressgateway-579c849b8d-ggsdz   1/1     Running   4 (28h ago)   2d5h
````

**O nosso ingress**, logo podemos entender que os gateways configura o Ingress.
Ele avisa ao ingress para expor suas portas com determinadas regras.

Confira a documentação para ter mais entendimento sobre o assunto.

De modo simples o gateway precisa ter o selector que aponta para um ou mais ingress, mas geralmente é um.
Pelo menos um server é necessário ser especificado, sendo que o server precisa ter pelo menos uma porta  (com numero, protocolo e nome) e um host, que é em tese um service do nosso cluster k8s.
