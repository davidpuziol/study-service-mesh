
## ADDons no Istio

Vamos instalar alguns addons no istio para monitoramento da nossa rede mesh. Com eles e outros mais voce pode analisar graficamente os traces, topologia, telemetria, saude da aplicação, etc.
Esses addons nada mais são do que outra ferramentas que são deployadas com manifestos no k8s e utilizam os dados entregues ao control plane pelos envoys e reunidos de forma espetacular para o nosso beneficio.
Para estas ferramentas o istioctl ja tem algumas chamadas implementadas que nos tras direto os endpoints desses servicos.

- [Kiali](https://kiali.io/) Vale a pena conferir a documentaçao, pois é possivel customizar a instalação. O Kiale serve para acompanhar as aplicações em tempo real
  - Topologia
  - Saúde
  - Detalhes
  - Rastreamento
  - Validações
  - Assistentes
  - Configuração
- [Grafana](https://grafana.com/) (dashboard para métricas)
- [Prometheus](https://prometheus.io/)\
  - colhe as métricas em série temporal
  - cria alertas
- [Jaeger](https://www.jaegertracing.io/) (para tracing fim a fim)
- [skywalking](https://skywalking.apache.org/) (Ferramenta mais top para visualização em tempo real do aplicativo, concorrente do kiali, mas é mais complicado a implantação, ainda sim vale a pena conferir)

Vamos adicionar todos os addons da pasta sample que já vem com o istio
````bash
❯ kubectl apply -f istio-*/samples/addons/
serviceaccount/grafana created
configmap/grafana created
service/grafana created
deployment.apps/grafana created
configmap/istio-grafana-dashboards created
configmap/istio-services-grafana-dashboards created
deployment.apps/jaeger created
service/tracing created
service/zipkin created
service/jaeger-collector created
serviceaccount/kiali created
configmap/kiali created
clusterrole.rbac.authorization.k8s.io/kiali-viewer created
clusterrole.rbac.authorization.k8s.io/kiali created
clusterrolebinding.rbac.authorization.k8s.io/kiali created
role.rbac.authorization.k8s.io/kiali-controlplane created
rolebinding.rbac.authorization.k8s.io/kiali-controlplane created
service/kiali created
deployment.apps/kiali created
serviceaccount/prometheus created
configmap/prometheus created
clusterrole.rbac.authorization.k8s.io/prometheus created
clusterrolebinding.rbac.authorization.k8s.io/prometheus created
service/prometheus created
deployment.apps/prometheus created
````

o comando istioctl dashboard já faz a chamada direto para alguns paineis.

Vá no painel do kiali e brinque um pouco, assim como no painel do grafana prometheus e jaeger

````bash
istioctl dashboard kiali
istioctl dashboard prometheus
istioctl dashboard grafana
istioctl dashboard jaeger
````

para gerar algum trafego execute o comando abaixo

`````bash
for i in $(seq 1 300); do curl -s -o /dev/null "http://$GATEWAY_URL/productpage"; done
````
