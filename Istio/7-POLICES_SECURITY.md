# Polices and Securities
<https://istio.io/latest/docs/concepts/security/>

Microserviços tem suas vantagens, mas trás alguns problemas que devem ser corrigidos.

- Defender contra ataques man-in-the-middle, eles precisam de criptografia de tráfego.

- Fornecer controle de acesso de serviço flexível, eles precisam de TLS mútuo e políticas de acesso refinadas.

- Para determinar quem fez o quê e em que momento, eles precisam de ferramentas de auditoria.

Asegurança do Istio mitiga ameaças internas e externas contra seus dados, endpoints, comunicação e plataforma.

Os recursos de segurança do Istio fornecem identidade forte, política poderosa, criptografia TLS transparente e ferramentas de autenticação, autorização e auditoria (AAA) para proteger seus serviços e dados. Os objetivos da segurança do Istio são:

- Segurança por padrão: não são necessárias alterações no código e na infraestrutura do aplicativo
- Defesa em profundidade: integre-se aos sistemas de segurança existentes para fornecer várias camadas de defesa
- Rede de confiança zero: crie soluções de segurança em redes não confiáveis

## Architetura

![](../pics/archsecurity.jpg)

Para segurança no istio alguns componentes existem.

- Uma Autoridade Certificadora (CA) própria para gerenciamento de chaves
- Os envoys recebem do control plane as políticas abaixo e funcionam como pontos de aplicação delas para proteger a comunicação.
  - Politicas de autenticação
  - Politicas de autorização
  - Informações de nomenclatura segura


## Policies
Com as polices vamos conseguir fazer o rate, para limitar acesso, trafego, whitelist e blacklist para restringer acesso ao serviço.

**Por default é possível acessar todos os serviço de qualquer namespace dentro do k8s** pois ele prove um dns interno da seguinte forma.

**nomedoserviço**.**namespace**.**svc**.**cluster**.**local**

Logo, se vc fizer um curl dentro de qualquer pod para o modelo do dns acima citado, vc consegue chegar lá.

O certo seria que cada pod e services estivem de forma isolada, e para isso é possível aplicar algumas regras usando polices.

## Cenário para testes

Vamos criar 3 namespaces
fazer o injection do istio somente nos dois primeiros

````bash
❯ k create ns tribulus                  
namespace/tribulus created

❯ k create ns terrestris
namespace/terrestris created

❯ k create ns tetris    
namespace/tetris created

❯ k label ns tribulus istio-injection=enabled                         
namespace/tribulus labeled

❯ k label ns terrestris istio-injection=enabled 
namespace/terrestris labeled

❯ k get ns                                     
NAME                 STATUS   AGE
default              Active   3d1h
istio-system         Active   3d
kube-node-lease      Active   3d1h
kube-public          Active   3d1h
kube-system          Active   3d1h
local-path-storage   Active   3d1h
terrestris           Active   85s
tetris               Active   80s
tribulus             Active   91s

❯ k get deployments.apps --all-namespaces   
NAMESPACE            NAME                     READY   UP-TO-DATE   AVAILABLE   AGE
istio-system         grafana                  1/1     1            1           2d14h
istio-system         istio-egressgateway      1/1     1            1           3d
istio-system         istio-ingressgateway     1/1     1            1           3d
istio-system         istiod                   1/1     1            1           3d
istio-system         jaeger                   1/1     1            1           2d14h
istio-system         kiali                    1/1     1            1           2d14h
istio-system         prometheus               1/1     1            1           2d14h
kube-system          coredns                  2/2     2            2           3d1h
local-path-storage   local-path-provisioner   1/1     1            1           3d1h

~/projects/study-service-mesh/Istio/istio-1.14.0 main !3 ?8                                                    ⎈ kind-kind  1.1.7 14:55:26
❯ k get pods --all-namespaces
NAMESPACE            NAME                                         READY   STATUS    RESTARTS         AGE
istio-system         grafana-78588947bf-6fsmg                     1/1     Running   4 (47h ago)      2d14h
istio-system         istio-egressgateway-5d67ff499d-7rwqh         1/1     Running   4 (47h ago)      3d
istio-system         istio-ingressgateway-579c849b8d-ggsdz        1/1     Running   4 (47h ago)      3d
istio-system         istiod-8cbfb5596-fzk99                       1/1     Running   6 (47h ago)      3d
istio-system         jaeger-b5874fcc6-278gh                       1/1     Running   4 (47h ago)      2d14h
istio-system         kiali-575cc8cbf-qw7c8                        1/1     Running   4 (47h ago)      2d14h
istio-system         prometheus-6544454f65-jnqfk                  2/2     Running   8 (47h ago)      2d14h
kube-system          coredns-6d4b75cb6d-q79r4                     1/1     Running   4 (47h ago)      3d1h
kube-system          coredns-6d4b75cb6d-rxwz6                     1/1     Running   4 (47h ago)      3d1h
kube-system          etcd-kind-control-plane                      1/1     Running   1 (47h ago)      47h
kube-system          kindnet-7gpn4                                1/1     Running   4 (47h ago)      3d1h
kube-system          kindnet-q6psf                                1/1     Running   4 (47h ago)      3d1h
kube-system          kindnet-qr4pv                                1/1     Running   4 (47h ago)      3d1h
kube-system          kube-apiserver-kind-control-plane            1/1     Running   1 (47h ago)      47h
kube-system          kube-controller-manager-kind-control-plane   1/1     Running   14 (4h35m ago)   3d1h
kube-system          kube-proxy-m7pfw                             1/1     Running   4 (47h ago)      3d1h
kube-system          kube-proxy-q28t6                             1/1     Running   4 (47h ago)      3d1h
kube-system          kube-proxy-zpl2j                             1/1     Running   4 (47h ago)      3d1h
kube-system          kube-scheduler-kind-control-plane            1/1     Running   14 (4h35m ago)   3d1h
local-path-storage   local-path-provisioner-9cd9bd544-mc5q2       1/1     Running   5 (47h ago)      3d1h
````

vamos deployar agora dois exemplo que tempos o httpbin e o slee em todos os nosso namepsaces criados


````bash
❯ k apply -f samples/httpbin/httpbin.yaml -n terrestris 
serviceaccount/httpbin created
service/httpbin created
deployment.apps/httpbin created

❯ k apply -f samples/httpbin/httpbin.yaml -n tetris    
serviceaccount/httpbin created
service/httpbin created
deployment.apps/httpbin created
````

Vamos aplicar tb sample sleep.

````bash
❯ k apply -f samples/sleep/sleep.yaml -n tribulus 
serviceaccount/sleep created
service/sleep created
deployment.apps/sleep created

❯ k apply -f samples/sleep/sleep.yaml -n terrestris 
serviceaccount/sleep created
service/sleep created
deployment.apps/sleep created

❯ k apply -f samples/sleep/sleep.yaml -n tetris    
serviceaccount/sleep created
service/sleep created
````

````bash
❯ k get pods --all-namespaces 
NAMESPACE            NAME                                         READY   STATUS    RESTARTS         AGE
istio-system         grafana-78588947bf-6fsmg                     1/1     Running   4 (2d ago)       2d14h
istio-system         istio-egressgateway-5d67ff499d-7rwqh         1/1     Running   4 (2d ago)       3d1h
istio-system         istio-ingressgateway-579c849b8d-ggsdz        1/1     Running   4 (2d ago)       3d1h
istio-system         istiod-8cbfb5596-fzk99                       1/1     Running   6 (2d ago)       3d1h
istio-system         jaeger-b5874fcc6-278gh                       1/1     Running   4 (2d ago)       2d14h
istio-system         kiali-575cc8cbf-qw7c8                        1/1     Running   4 (2d ago)       2d14h
istio-system         prometheus-6544454f65-jnqfk                  2/2     Running   8 (2d ago)       2d14h
kube-system          coredns-6d4b75cb6d-q79r4                     1/1     Running   4 (2d ago)       3d1h
kube-system          coredns-6d4b75cb6d-rxwz6                     1/1     Running   4 (2d ago)       3d1h
kube-system          etcd-kind-control-plane                      1/1     Running   1 (2d ago)       2d
kube-system          kindnet-7gpn4                                1/1     Running   4 (2d ago)       3d1h
kube-system          kindnet-q6psf                                1/1     Running   4 (2d ago)       3d1h
kube-system          kindnet-qr4pv                                1/1     Running   4 (2d ago)       3d1h
kube-system          kube-apiserver-kind-control-plane            1/1     Running   1 (2d ago)       2d
kube-system          kube-controller-manager-kind-control-plane   1/1     Running   14 (4h47m ago)   3d1h
kube-system          kube-proxy-m7pfw                             1/1     Running   4 (2d ago)       3d1h
kube-system          kube-proxy-q28t6                             1/1     Running   4 (2d ago)       3d1h
kube-system          kube-proxy-zpl2j                             1/1     Running   4 (2d ago)       3d1h
kube-system          kube-scheduler-kind-control-plane            1/1     Running   14 (4h47m ago)   3d1h
local-path-storage   local-path-provisioner-9cd9bd544-mc5q2       1/1     Running   5 (2d ago)       3d1h
terrestris           httpbin-847f64cc8d-6ktzw                     2/2     Running   0                6m23s
terrestris           sleep-5887ccbb67-fvmml                       2/2     Running   0                3m
tetris               httpbin-847f64cc8d-qcbcc                     1/1     Running   0                6m19s
tetris               sleep-5887ccbb67-lxnfd                       1/1     Running   0                2m56s
tribulus             httpbin-847f64cc8d-zwdjb                     2/2     Running   0                6m33s
tribulus             sleep-5887ccbb67-kxh4c                       2/2     Running   0                3m4s
````

nas 6 ultimas linhas podemos conferir que o sidecar do envoy foi aplicado nos pods do namespace tribulus e terrestris, mas não foi aplicado no tetris como definimos. Tudo certo!

````bash
❯ k get svc --all-namespaces                            
NAMESPACE      NAME                   TYPE           CLUSTER-IP      EXTERNAL-IP   PORT(S)                                                                      AGE
default        kubernetes             ClusterIP      10.96.0.1       <none>        443/TCP                                                                      3d2h
istio-system   grafana                ClusterIP      10.96.99.160    <none>        3000/TCP                                                                     2d14h
istio-system   istio-egressgateway    ClusterIP      10.96.10.173    <none>        80/TCP,443/TCP                                                               3d1h
istio-system   istio-ingressgateway   LoadBalancer   10.96.149.13    <pending>     15021:32076/TCP,80:31498/TCP,443:31818/TCP,31400:32020/TCP,15443:30134/TCP   3d1h
istio-system   istiod                 ClusterIP      10.96.230.2     <none>        15010/TCP,15012/TCP,443/TCP,15014/TCP                                        3d1h
istio-system   jaeger-collector       ClusterIP      10.96.1.232     <none>        14268/TCP,14250/TCP,9411/TCP                                                 2d14h
istio-system   kiali                  ClusterIP      10.96.180.49    <none>        20001/TCP,9090/TCP                                                           2d14h
istio-system   prometheus             ClusterIP      10.96.40.75     <none>        9090/TCP                                                                     2d14h
istio-system   tracing                ClusterIP      10.96.164.75    <none>        80/TCP,16685/TCP                                                             2d14h
istio-system   zipkin                 ClusterIP      10.96.120.55    <none>        9411/TCP                                                                     2d14h
kube-system    kube-dns               ClusterIP      10.96.0.10      <none>        53/UDP,53/TCP,9153/TCP                                                       3d2h
terrestris     httpbin                ClusterIP      10.96.58.244    <none>        8000/TCP                                                                     8m15s
terrestris     sleep                  ClusterIP      10.96.157.82    <none>        80/TCP                                                                       4m52s
tetris         httpbin                ClusterIP      10.96.207.12    <none>        8000/TCP                                                                     8m11s
tetris         sleep                  ClusterIP      10.96.240.214   <none>        80/TCP                                                                       4m48s
tribulus       httpbin                ClusterIP      10.96.91.126    <none>        8000/TCP                                                                     8m25s
tribulus       sleep                  ClusterIP      10.96.115.0     <none>        80/TCP                                                                       4m56s
````

service tudo funcionando!

## Política de autenticação
<https://istio.io/latest/docs/tasks/security/authentication/authn-policy/>

Lembra que eu falei que era possível acessar todos os serviços do k8s usando somente um dns interno dele?

Poderíamos ir em qualquer pod sleep de qualquer namespace e fazer os comandos, issó é só para mostrar que ele consegue chegar através do dns

````bash
❯ k exec -ti -n tribulus sleep-5887ccbb67-kxh4c -- curl http://httpbin.tetris:8000/ip -s -o /dev/null -w "%{http_code}\n" 
200

❯ k exec -ti -n tribulus sleep-5887ccbb67-kxh4c -- curl http://httpbin.terrestris:8000/ip -s -o /dev/null -w "%{http_code}\n"
200

❯ k exec -ti -n tribulus sleep-5887ccbb67-kxh4c -- curl http://httpbin.tribulus:8000/ip -s -o /dev/null -w "%{http_code}\n"
200
````

testando a comunicação de todos com todos

````bash
❯ for from in "tribulus" "terrestris" "tetris"; do for to in "tribulus" "terrestris" "tetris"; do kubectl exec "$(kubectl get pod -l app=sleep -n ${from} -o jsonpath={.items..metadata.name})" -c sleep -n ${from} -- curl -s "http://httpbin.${to}:8000/ip" -s -o /dev/null -w "sleep.${from} to httpbin.${to}: %{http_code}\n"; done; done

sleep.tribulus to httpbin.tribulus: 200
sleep.tribulus to httpbin.terrestris: 200
sleep.tribulus to httpbin.tetris: 200
sleep.terrestris to httpbin.tribulus: 200
sleep.terrestris to httpbin.terrestris: 200
sleep.terrestris to httpbin.tetris: 200
sleep.tetris to httpbin.tribulus: 200
sleep.tetris to httpbin.terrestris: 200
sleep.tetris to httpbin.tetris: 200
````

Testando se temos alguma política de autenticaçao

````bash
❯ kubectl get peerauthentication --all-namespaces
No resources found

❯ kubectl get destinationrules.networking.istio.io --all-namespaces -o yaml | grep "host:"

````

Fazendo uns testes. Vamos disparar um curl do pods sleep em tribulus que é gerenciado pelo istio para o httpbin no namespaces terrestris e olhar se o cabeçalho tem o X-Forwarded-Client-Cert e depois vamos fazer o mesmo para o namespace tetris que não é gerenciado pelo istio, ou seja, não tem o envoy. 

k exec -ti -n tribulus sleep-5887ccbb67-kxh4c -- curl -s http://httpbin.terrestris:8000/headers -s | grep X-Forwarded-Client-Cert

k exec -ti -n tribulus sleep-5887ccbb67-kxh4c -- curl http://httpbin.tetris:8000/headers -s | grep X-Forwarded-Client-Cert

````bash
❯ k exec -ti -n tribulus sleep-5887ccbb67-kxh4c -- curl -s http://httpbin.terrestris:8000/headers -s | grep X-Forwarded-Client-Cert
    "X-Forwarded-Client-Cert": "By=spiffe://cluster.local/ns/terrestris/sa/httpbin;Hash=0d9ff68bccae66fe460d7bebf3bc3cf08a5c80d43fe69795323727f0906845c3;Subject=\"\";URI=spiffe://cluster.local/ns/tribulus/sa/sleep"

~/projects/study-service-mesh/Istio/istio-1.14.0 main !3 ?8                                                                                           ⎈ kind-kind  1.1.7 18:32:32
❯ k exec -ti -n tribulus sleep-5887ccbb67-kxh4c -- curl http://httpbin.tetris:8000/headers -s | grep X-Forwarded-Client-Cert

❯ 
````

Não foi encontrado o certificado no httpbin rodando no namespace tetris, ou seja, não é uma comunicação com tls.


### Habilitar globalmente o mutual TLS

Quando aplicamos o mutual tls no namespace istio-system todos os sidecars de todos os namespaces exigirão comunicação com tls.

É possível aplicar somente em algum namespace como nos exemplos da documentação mas são casos muito excepcionais, prefiro aplicar a tudo e somente cancelar as cargas de trabalhaos aproveitando a precedência.

Existe uma precedência de política. Uma política aplicadas a um determinado pod tem precedência a uma política aplicada a um namespace. Logo com essa informação acho muito mais interssante aplicar o mutual tls a tudo e somente desativar quando necessário.

Aplicando a tudo e conferindo a comunicação geral entre todos eles

````bash
$ kubectl apply -f - <<EOF
apiVersion: security.istio.io/v1beta1
kind: PeerAuthentication
metadata:
  name: "default"
  namespace: "istio-system"
spec:
  mtls:
    mode: STRICT
EOF

❯ for from in "tribulus" "terrestris" "tetris"; do for to in "tribulus" "terrestris" "tetris"; do kubectl exec "$(kubectl get pod -l app=sleep -n ${from} -o jsonpath={.items..metadata.name})" -c sleep -n ${from} -- curl -s "http://httpbin.${to}:8000/ip" -s -o /dev/null -w "sleep.${from} to httpbin.${to}: %{http_code}\n"; done; done
sleep.tribulus to httpbin.tribulus: 200
sleep.tribulus to httpbin.terrestris: 200
sleep.tribulus to httpbin.tetris: 200
sleep.terrestris to httpbin.tribulus: 200
sleep.terrestris to httpbin.terrestris: 200
sleep.terrestris to httpbin.tetris: 200
sleep.tetris to httpbin.tribulus: 000
command terminated with exit code 56
sleep.tetris to httpbin.terrestris: 000
command terminated with exit code 56
sleep.tetris to httpbin.tetris: 200
````

Observe que em tetris nao conseguiu chegar nos httpbin que estão com sidecars que exigem tls.
Mas poderíamos desativar o tls somente no httpbin do namespace terrestris para conferir a precedencia.

````bash
❯ cat <<EOF | kubectl apply -n terrestris -f -
apiVersion: security.istio.io/v1beta1
kind: PeerAuthentication
metadata:
  name: "httpbin-no-tls"
  namespace: "terrestris"
spec:
  selector:
    matchLabels:
      app: httpbin
  mtls:
    mode: DISABLE
EOF
peerauthentication.security.istio.io/httpbin-no-tls created

❯ for from in "tribulus" "terrestris" "tetris"; do for to in "tribulus" "terrestris" "tetris"; do kubectl exec "$(kubectl get pod -l app=sleep -n ${from} -o jsonpath={.items..metadata.name})" -c sleep -n ${from} -- curl -s "http://httpbin.${to}:8000/ip" -s -o /dev/null -w "sleep.${from} to httpbin.${to}: %{http_code}\n"; done; done
sleep.tribulus to httpbin.tribulus: 200
sleep.tribulus to httpbin.terrestris: 200
sleep.tribulus to httpbin.tetris: 200
sleep.terrestris to httpbin.tribulus: 200
sleep.terrestris to httpbin.terrestris: 200
sleep.terrestris to httpbin.tetris: 200
sleep.tetris to httpbin.tribulus: 000
command terminated with exit code 56
sleep.tetris to httpbin.terrestris: 200
sleep.tetris to httpbin.tetris: 200
````

Veja que httpbin respondeu e só tivemos um code 56, porém o namespace terrestris ainda tem o mutual tls ativado, menos os envoys do pod httpbin desse namespace.

````bash
k delete peerauthentications.security.istio.io -n terrestris httpbin-no-tls
````

é possivel aplicar um destination rule forcando o tls para algum namespace ou pod caso usassemos um selector

````bash
cat <<EOF | kubectl apply -f -
apiVersion: networking.istio.io/v1alpha3
kind: DestinationRule
metadata:
  name: "default"
  namespace: "istio-system"
spec:
  host: "*.local"
  trafficPolicy:
    tls:
      mode: ISTIO_MUTUAL
EOF
````

Conferindo agora, veja que tomamos 503 ao inves do 56. Essa é uma configuração que vale a pena manter até mesmo em produção

````bash
for from in "tribulus" "terrestris" "tetris"; do for to in "tribulus" "terrestris" "tetris"; do kubectl exec "$(kubectl get pod -l app=sleep -n ${from} -o jsonpath={.items..metadata.name})" -c sleep -n ${from} -- curl -s "http://httpbin.${to}:8000/ip" -s -o /dev/null -w "sleep.${from} to httpbin.${to}: %{http_code}\n"; done; done
sleep.tribulus to httpbin.tribulus: 200
sleep.tribulus to httpbin.terrestris: 200
sleep.tribulus to httpbin.tetris: 503
sleep.terrestris to httpbin.tribulus: 200
sleep.terrestris to httpbin.terrestris: 200
sleep.terrestris to httpbin.tetris: 503
sleep.tetris to httpbin.tribulus: 000
command terminated with exit code 56
sleep.tetris to httpbin.terrestris: 000
command terminated with exit code 56
sleep.tetris to httpbin.tetris: 200
````

Agora vamos liberar criar uma regra dizendo que a comunicação para o httpbin do tetris seja permitida sem o mutual tls

````bash
cat <<EOF | kubectl apply -f -
apiVersion: networking.istio.io/v1alpha3
kind: DestinationRule
metadata:
  name: httpbin-tetris
  namespace: tetris
spec:
  host: httpbin.tetris.svc.cluster.local
  trafficPolicy:
    tls:
      mode: DISABLE
EOF
````

Veja que acabou permitindo agora com o 200

````bash
❯ for from in "tribulus" "terrestris" "tetris"; do for to in "tribulus" "terrestris" "tetris"; do kubectl exec "$(kubectl get pod -l app=sleep -n ${from} -o jsonpath={.items..metadata.name})" -c sleep -n ${from} -- curl -s "http://httpbin.${to}:8000/ip" -s -o /dev/null -w "sleep.${from} to httpbin.${to}: %{http_code}\n"; done; done
sleep.tribulus to httpbin.tribulus: 200
sleep.tribulus to httpbin.terrestris: 200
sleep.tribulus to httpbin.tetris: 200
sleep.terrestris to httpbin.tribulus: 200
sleep.terrestris to httpbin.terrestris: 200
sleep.terrestris to httpbin.tetris: 200
sleep.tetris to httpbin.tribulus: 000
command terminated with exit code 56
sleep.tetris to httpbin.terrestris: 000
command terminated with exit code 56
sleep.tetris to httpbin.tetris: 200
````
Pode deletar tudo para o proximo passo

### Autorizathon Police

<https://istio.io/latest/docs/reference/config/security/authorization-policy/>

Vamos configurar um controle de acesso para os nosso worksloads.

Primeiro, você configura uma política simples que rejeita todas as solicitações aos workloads, em seguida, concede mais acesso forma gradual e incremental.

Como não tem um selector para algum worksload específico, será aplicado a todo o namespace default. Nenhum tráfego será permiido. Como todo o nosso bookinfo está lá, nada será acessado.

````bash
kubectl apply -f - <<EOF
apiVersion: security.istio.io/v1beta1
kind: AuthorizationPolicy
metadata:
  name: allow-nothing
  namespace: default
spec:
  {}
EOF
````

Logo de cara vamos tomar um rbac acess denied, o que mostra que a politica deny-all está funcionando.

vamos liberar o acesso a productpage. O comando a seguir para criará uma politica chamada productpage-viewer para permitir acesso com o método GET à productpage carga. A política não define o **from** campo **rules** que significa que todas as fontes são permitidas, permitindo efetivamente que todos os usuários e cargas de trabalho: Você conseguirá chegar até a pagina productpage mas ela não estará completa pois os outros worksloads darão permissão negada.

````bash
kubectl apply -f - <<EOF
apiVersion: security.istio.io/v1beta1
kind: AuthorizationPolicy
metadata:
  name: "productpage-viewer"
  namespace: default
spec:
  selector:
    matchLabels:
      app: productpage
  action: ALLOW
  rules:
  - to:
    - operation:
        methods: ["GET"]
EOF
````

Vamos agora liberar o acesso ao restante porém vindo somente da service account do productpage

````bash
 k get sa  
NAME                   SECRETS   AGE
bookinfo-details       0         24h
bookinfo-productpage   0         24h
bookinfo-ratings       0         24h
bookinfo-reviews       0         24h
default                0         4d13h

> k describe sa bookinfo-productpage        
Name:                bookinfo-productpage
Namespace:           default
Labels:              account=productpage
Annotations:         <none>
Image pull secrets:  <none>
Mountable secrets:   <none>
Tokens:              <none>
Events:              <none>
````

sabemos que essa service account esta no namespace deafult, com nome de bookinfo-productpage.

Pra apontar para esse service account precisamos utilizar esse padrão:
**cluster.local/ns/default/sa/bookinfo-productpage**

Vamos liberar nos workloads details, o que vem do service account do productpage. Objeserve que principals é uma lista de service accounts.

````bash
kubectl apply -f - <<EOF
apiVersion: security.istio.io/v1beta1
kind: AuthorizationPolicy
metadata:
  name: "details-viewer"
  namespace: default
spec:
  selector:
    matchLabels:
      app: details
  action: ALLOW
  rules:
  - from:
    - source:
        principals: ["cluster.local/ns/default/sa/bookinfo-productpage"]
    to:
    - operation:
        methods: ["GET"]
EOF
````

Nesse ponto parte do serviço já começou a aparecer.

o product page acessa o review e o review acessa o rattings, então vamos liberando..

````bash
❯ kubectl apply -f - <<EOF
apiVersion: security.istio.io/v1beta1
kind: AuthorizationPolicy
metadata:
  name: "reviews-viewer"
  namespace: default
spec:
  selector:
    matchLabels:
      app: reviews
  action: ALLOW
  rules:
  - from:
    - source:
        principals: ["cluster.local/ns/default/sa/bookinfo-productpage"]
    to:
    - operation:
        methods: ["GET"]
EOF
authorizationpolicy.security.istio.io/reviews-viewer created

❯ kubectl apply -f - <<EOF
apiVersion: security.istio.io/v1beta1
kind: AuthorizationPolicy
metadata:
  name: "ratings-viewer"
  namespace: default
spec:
  selector:
    matchLabels:
      app: ratings
  action: ALLOW
  rules:
  - from:
    - source:
        principals: ["cluster.local/ns/default/sa/bookinfo-reviews"]
    to:
    - operation:
        methods: ["GET"]
EOF
authorizationpolicy.security.istio.io/ratings-viewer created
````

Agora tudo voltou a funcionar.

Nõs apliamos por serviço, mas poderíamos ter aplicado a todo namespace de uma vez e todo mundo do namespace poderia ter acesso ao serviço. 

````bash
❯  kubectl apply -f - <<EOF
apiVersion: security.istio.io/v1beta1
kind: AuthorizationPolicy
metadata:
  name: bookinfo
  namespace: default
spec:
  action: ALLOW
  rules:
  - from:
    - source:
        namespaces: ["istio-system","default"]
  - to:
    - operation:
        methods: ["GET"]
EOF
authorizationpolicy.security.istio.io/bookinfo configured
````

O istio-system foi necessário pois a requisição que chega no productpage vem do ingress que esta no istio-system.