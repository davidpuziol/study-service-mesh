# Virtual Services

<https://istio.io/latest/docs/reference/config/networking/virtual-service/>

A maior parte da configuração costuma estar nos virtual services. Logo será a maior parte do nosso estudo, mas aqui trará somente um overview.

O virtual Service é a configuração do roteamento de tráfego do nosso gateway. Quando chega uma requisição para o host X na porta Y o que ele deve fazer? Para onde deve ir?

**A VirtualServicedefine um conjunto de regras de roteamento de tráfego a serem aplicadas quando um host é endereçado. Cada regra de roteamento define critérios de correspondência para o tráfego de um protocolo específico. Se o tráfego for correspondido, ele será enviado para um serviço de destino nomeado (ou subconjunto/versão dele) definido no registro.**

É possível que muitos serviços escutem na porta 80 por exemplo ou 443, logo como saber para quem redirecionar? Quem faz esse tráfego é o nosso virtual service baseado em regras

As regras são definidas em match. Se der match vai para a rota X que seria o service vinculado a um pod.

````bash
❯ k get virtualservices.networking.istio.io
NAME       GATEWAYS               HOSTS   AGE
bookinfo   ["bookinfo-gateway"]   ["*"]   2d3h
❯ k get virtualservices.networking.istio.io -o yaml
````

````yaml
apiVersion: v1
items:
- apiVersion: networking.istio.io/v1beta1
  kind: VirtualService
  metadata:
    annotations:
      kubectl.kubernetes.io/last-applied-configuration: |
        {"apiVersion":"networking.istio.io/v1alpha3","kind":"VirtualService","metadata":{"annotations":{},"name":"bookinfo","namespace":"default"},"spec":{"gateways":["bookinfo-gateway"],"hosts":["*"],"http":[{"match":[{"uri":{"exact":"/productpage"}},{"uri":{"prefix":"/static"}},{"uri":{"exact":"/login"}},{"uri":{"exact":"/logout"}},{"uri":{"prefix":"/api/v1/products"}}],"route":[{"destination":{"host":"productpage","port":{"number":9080}}}]}]}}
    creationTimestamp: "2022-06-07T22:41:32Z"
    generation: 1
    name: bookinfo
    namespace: default
    resourceVersion: "38745"
    uid: b037b12f-e53c-481a-87fd-6a22beb3d418
  spec:
    gateways:
    - bookinfo-gateway
    hosts:
    - '*'
    http:
    - match:
      - uri:
          exact: /productpage
      - uri:
          prefix: /static
      - uri:
          exact: /login
      - uri:
          exact: /logout
      - uri:
          prefix: /api/v1/products
      route:
      - destination:
          host: productpage
          port:
            number: 9080
kind: List
metadata:
  resourceVersion: ""
  selfLink: ""
````

Veja que ele tem a rota se der match com alguma regra acima ele vai para o service productpage na porta 9080

É possível que no match nos utilizemos muitas outras coisas além da uri, mas também headers, user, etc

Se vc defini um service de resposta como fizemos em

````yaml
      route:
      - destination:
          host: productpage
          port:
            number: 9080
````

E esse host tiver mais um pod que responde na requisição, ele fará round robin por padrão, que é um balancemento de carga default do k8s. Por isso que vemos as 3 versoes do review sendo mostradas aleatoriamente sem nenhum controle.

Essas 3 versões tem o mesmo label que foi aplicado o filtro no service.
Vamos analisar o service do review para entende melhor.

````yaml
❯ k get services reviews -o yaml
apiVersion: v1
kind: Service
metadata:
  annotations:
    kubectl.kubernetes.io/last-applied-configuration: |
      {"apiVersion":"v1","kind":"Service","metadata":{"annotations":{},"labels":{"app":"reviews","service":"reviews"},"name":"reviews","namespace":"default"},"spec":{"ports":[{"name":"http","port":9080}],"selector":{"app":"reviews"}}}
  creationTimestamp: "2022-06-08T16:36:53Z"
  labels:
    app: reviews
    service: reviews
  name: reviews
  namespace: default
  resourceVersion: "103603"
  uid: 2bf33d8b-47c1-44c5-820d-69c03dd7ae5d
spec:
  clusterIP: 10.96.173.59
  clusterIPs:
  - 10.96.173.59
  internalTrafficPolicy: Cluster
  ipFamilies:
  - IPv4
  ipFamilyPolicy: SingleStack
  ports:
  - name: http
    port: 9080
    protocol: TCP
    targetPort: 9080
  # DENTRO DOS PODS QUE RESPONDEM AO SERVICE TB TEM A LABEL VERSION, MAS ELE SÓ FILTROU PELA LABEL APP, LOGO OS 3 VÃO RESPONDER  
  selector:
    app: reviews
  sessionAffinity: None
  type: ClusterIP
status:
  loadBalancer: {}
````

Vamos conferir....

````bash
❯ k get pods -l app=reviews
NAME                          READY   STATUS    RESTARTS      AGE
reviews-v1-68b4dcbdb9-g8c42   2/2     Running   4 (33h ago)   33h
reviews-v2-565bcd7987-sgphm   2/2     Running   4 (33h ago)   33h
reviews-v3-d88774f9c-4dqbm    2/2     Running   4 (33h ago)   33h
````

Agora vem a pergunta..
Pq nao funcionou os outros matchs?

## Protocolos usados
<https://istio.io/latest/docs/reference/config/networking/virtual-service/#VirtualService>
O virtual service pode ter alguns boloco para resolver determinados tipos de protocolos:

- **http**: rotas aplicadas aos protocolos HTTP/HTTP2/GRPC/TLS-terminated-HTTPS
  - conhecer o protocolo http é importante para conseguir filtrar bem as coisas <https://developer.mozilla.org/en-US/docs/Web/HTTP>
- **tls**: rotas aplicadas aos protocolos HTTPS/TLS 
  - <https://en.wikipedia.org/wiki/Transport_Layer_Security>
- **TCP**: aplicado a qualquer outra rota que não esteja em http ou tls.


## Destination Rule

<https://istio.io/latest/docs/reference/config/networking/destination-rule/>

Define as políticas que se aplicam ao tráfego destinado a um serviço após a ocorrência do roteamento no **virtual service**. Essas regras especificam a configuração para balanceamento de carga, tamanho do pool de conexão do sidecar e configurações de detecção de valores discrepantes para detectar e remover hosts não íntegros do pool de balanceamento de carga. Por exemplo, uma política simples de balanceamento de carga para o serviço de classificações teria a seguinte aparência:

Ao invés do virtual service disparar para um determinado serviço, irá entregar para o destination rule e este aplicará mais uma camada de filtros, para que depois ele saiba filtrar quais pods devem responder a requisição. Lembra que um service tem o seu próprio loadbalancer e aplica round robin para entregar para os diferenetes pods. Porém podemos retirar essa função do service ganhar mais poderemos com o **destination rule**.

vamos aplicar na nossa configuração o seguinte

o virtual service do momento tem isso em destination, e somente temos que dá o match e chama direto com essa rota.

````yaml
    route:
    - destination:
        host: productpage
        port:
          number: 9080
````

Agora vamos criar um virtual service para cada um dos nossos microservices e somente entrgar o trafego para quem tem versao 1 = v1

````yaml
#cat Istio/istio-1.14.0/samples/bookinfo/networking/virtual-service-all-v1.yaml
❯ cat virtual-service-all-v1.yaml                                             
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: productpage
spec:
  hosts:
  - productpage
  http:
  - route:
    - destination:
        host: productpage
        # veja que ao inez que ir para um service estamos indo para um destinationrule
        subset: v1
---
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: reviews
spec:
  hosts:
  - reviews
  http:
  - route:
  # quando definimos mais de um destination é necessário colocar a distribuicao que toda ela deve somar 100
    - destination:
        host: reviews
        subset: v2
      weight: 30
    - destination:
        host: reviews
        subset: v3
      weight: 70
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: ratings
spec:
  hosts:
  - ratings
  http:
  - route:
    - destination:
        host: ratings
        subset: v1
---
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: details
spec:
  hosts:
  - details
  http:
  - route:
    - destination:
        host: details
        subset: v1
---
````

````bash
❯ k apply -f Istio/istio-1.14.0/samples/bookinfo/networking/virtual-service-all-v1.yaml 
virtualservice.networking.istio.io/productpage created
virtualservice.networking.istio.io/reviews created
virtualservice.networking.istio.io/ratings created
virtualservice.networking.istio.io/details created

❯ k get virtualservices.networking.istio.io                                            
NAME          GATEWAYS               HOSTS             AGE
bookinfo      ["bookinfo-gateway"]   ["*"]             2d5h
details                              ["details"]       15s
productpage                          ["productpage"]   15s
ratings                              ["ratings"]       15s
reviews                              ["reviews"]       15s
````

A aplicação vai quebrar, pois não temos definido ainda os destination rules apontados, então vamos criá-los.

````yaml
apiVersion: networking.istio.io/v1alpha3
kind: DestinationRule
metadata:
  name: productpage
spec:
  # obseve que o host aqui é mesmo quando definido no virtual service, mas estamos filtrando a label
  host: productpage
  subsets:
  - name: v1
    labels:
      version: v1
---
apiVersion: networking.istio.io/v1alpha3
kind: DestinationRule
metadata:
  name: reviews
spec:
  host: reviews
  subsets:
  - name: v1
    labels:
      version: v1
  - name: v2
    labels:
      version: v2
  - name: v3
    labels:
      version: v3
---
apiVersion: networking.istio.io/v1alpha3
kind: DestinationRule
metadata:
  name: ratings
spec:
  host: ratings
  subsets:
  - name: v1
    labels:
      version: v1
  - name: v2
    labels:
      version: v2
---
apiVersion: networking.istio.io/v1alpha3
kind: DestinationRule
metadata:
  name: details
spec:
  host: details
  subsets:
  - name: v1
    labels:
      version: v1
  - name: v2
    labels:
      version: v2
---
````

Nesse ponto a aplicação somente funciona com o v2 e v3 para review. Vamos conferir no kiali.

![DestinationRule](../pics/destinationrule.jpg)
Veja que o trafego de v2 e v3 esta em media 30 para v2 e 70 para v3 como definido no virtualservice

Vamos adicionar um match no virtual service do review para que ele faça novamente um filtro depois do match que já passou pelo gateway. Dependendo do match ele vai para o destinationrule v2 ou v3.

## Virtual Service com Matchs

Um dos matchs mais utilizados é o HEADERSk e aqui vamos utilizar filtrar o header por um usuário chamado david. Se esse usuário for o que estiver logado, ele vai pra v2 ou v3 no virtualservice do review.

````yaml
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: reviews
spec:
  hosts:
  - reviews
  http:
  - match:
    - headers:
        end-user: 
          exact: david
    route:
    - destination:
        host: reviews
        subset: v2
      weight: 30
    - destination:
        host: reviews
        subset: v3
      weight: 70
````

Porém se nao der o match ele vai quebrar a aplicação pois não tem destino para ir. Nesse caso temos que adicionar uma nova rota. Vale lembrar que a ordem das rotas faz diferença nesse caso. Se colocassemos a rota para o v1 antes da rota com o match todo mundo iria pro v1.

````yaml
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: reviews
spec:
  hosts:
  - reviews
  http:
  #### este bloco para pegar a rota tem que dar o match ####
  - match:
    - headers:
        end-user: 
          exact: david
    route:
    - destination:
        host: reviews
        subset: v2
      weight: 30
    - destination:
        host: reviews
        subset: v3
      weight: 70
  #### este bloco para pegar a rota sem dar o match ####
  - route:
    - destination:
        host: reviews
        subset: v1
````

Seria possível com o uso do headers inclusive para filtrar a geo localizaçao para dar a rota com melhor latencia. Dá pra fazer muitas coisas com esse filtro.


## Simulando um problema na sua infra com virtual service

É possível injetar algum problema na sua infra para fazer um exercício simulado do que aconteceria.
Trabalhando no próprio virtual service do review, vamos injetar um delay para ver o que aconteceria.

<https://istio.io/latest/docs/reference/config/networking/virtual-service/#HTTPFaultInjection-Delay>
<https://istio.io/latest/docs/reference/config/networking/virtual-service/#HTTPFaultInjection-Abort>

````yaml
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: reviews
spec:
  hosts:
  - reviews
  http: #qual o tipo do protocolo
  - match:
    - headers:
        end-user: 
          exact: david
    route:
    - destination:
        host: reviews
        subset: v2
      weight: 30
    - destination:
        host: reviews
        subset: v3
      weight: 70
    fault: 
      delay:
        fixedDelay: 10s
        percentage: 
          value: 100
  - route:
    - destination:
        host: reviews
        subset: v1
````

com esse delay para o review tivemos problemas com a comunicação com o review. Veja o que aconteceu no monitoramento com o kaili.

![reviewdelay](../pics/delay.jpg)

Se baixamos para 2s o fixed delay temos

Veja que temos como mostrar atraves do kiali o tempo de 2s entre a requisção

![reviewdelay](../pics/delay2s.jpg)

O que fizmos aqui até agora usando a documentação?
<https://istio.io/latest/docs/tasks/traffic-management/request-timeouts/>
<https://istio.io/latest/docs/tasks/traffic-management/traffic-shifting/>
<https://istio.io/latest/docs/tasks/traffic-management/fault-injection/>

Fica a dica para implentar as outras coisas?

<https://istio.io/latest/docs/tasks/traffic-management/tcp-traffic-shifting/>
