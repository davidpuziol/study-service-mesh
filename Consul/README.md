# Consul

![ConsulArch](../pics/consul-logo.png)

<https://github.com/hashicorp/consul>

A Consul resolve os desafios que organizações de todos os tamanhos encontram com arquiteturas de microsserviços. Isso varia desde operar em vários ambientes distribuídos e localizações geográficas, até atender à necessidade de proteger todo o tráfego de aplicativos.

A rede de hoje deve se adaptar rapidamente e garantir que a comunicação seja criptografada o tempo todo. A Consul permite que as organizações adotem um modelo de confiança zero durante a expansão. A Consul pode conseguir tudo isso enquanto reduz a carga de operadores e desenvolvedores por meio da automação de tarefas cruciais de rede

- Desenvolvido em golang
- Tem um servidor dns embutido para ajudar a encontrar o services.
- Tem uma api para consulta
- Tem um heath check para conferir se o serviço esta no ativo
- Key Value embutido
- Tem um serviço de conexão segura entre os services
- Muito fácil de instalar

## Arquitetura
<https://www.consul.io/docs/architecture>
![ConsulArch](../pics/consularch.jpg)
\
\
**Agent**\
É o daemon que é executado em cada membro do cluster. Iniciado usando **consul agent**. Pode ser executado em modo client ou servidor e todos os nós são obrigados estarem executando este agent. O agente executa verificações e mantem os serviços sincronizados. Usam a interfaces DNS ou HTTP. Você deve executar um agente Consul por servidor ou host.
\
\
**Client**\
É um nó que executa o agent em modo client de forma minima e não guarda estado, somente escuta a atualização da rede LAN gossip. Logo que um agent em modo client é executado ele deve informar a um server quem ele é e quais serviços ele presta.
\
\
**Server**\
É um nó que executa o agent em modo server e possui a responsabilidade de manter o estado do cluster respondendo a consultas RPC feitas pelos clients.
\
\
Dentro de cada datacenter tem um lider, e este é responsável por processar todas as consultas e transações. É o único que contem todas as transações pois o restante do servers somente tem as informações dos seus respectivos pares. O lider é eleito automatomágicamente!
\
\
**Datacenter**\
Ambiente de rede privado e alta largura de banda sem atravessar a internet pública
\
\
**Gossip (Protocolo de fofoca)**\
Consul é construido em cima do Serf que é um protocolo Gossip completo usado para vários propositos. É como se fosse um pool que todos escutam para detecção de falhas e transmissão de eventos, mas baseado em UDP.
\
\
**Lan Gossip**\
é o pool gossip que contem nós que estão localizados na mesma rede local ou datacenter. Tanto os clients quantos os servers participam desse.
\
\
**Wan Gossip**\
é o pool que contem apenas servers e estão em diferentes datacenters ou redes locais. Somente os masters participam desse, mas não exclui de participar do lan.
\
\
**RPC**
Chamada de procedimento remoto. Este é o mecanimos de solicitação e resposta que permite um client faça solicitação a um server.

## Requisitos mínimos

O consul usa um protocolo de consenso para fornecer consistência de dados entre os servers. O protocolo de consenso é baseado em "Raft".

Para ter um ponto de falhar de acordo com o consensus é necessário pelo menos 3 servers e não 2 ou seja, este é o requisito mínimo para uma alta disponibilidade.
<https://www.consul.io/docs/architecture/consensus>

| Servers | Tamanho do Quorun | Tolerancia a falha |
|---|---|---|
| 1 | 1 | 0 |
| 2 | 2 | 0 |
| 3 | 2 | 1 |
| 4 | 3 | 1 |
| 5 | 3 | 2 |
| 6 | 4 | 2 |
| 7 | 4 | 3 |

As portas usadas são:

| SERVIÇO | PORTA | DESCRIÇÃO |
|---|---|----|
| DNS: The DNS server (TCP and UDP) | 8600 | Usada para resolver consultas DNS |
| HTTP: The HTTP API (TCP Only) | 8500 | Isso é usado pelos clientes para conversar com a API bem como para acessar a interface UI |
| HTTPS: The HTTPs API | disabled (8501)* | (opcional) Está desativada por padrão, mas a porta 8501 é uma convenção usada por várias ferramentas como padrão |
| gRPC: The gRPC API | disabled (8502)* | (opcional). Atualmente, o gRPC é usado apenas para expor a API xDS aos proxies Envoy. Está desativado por padrão, mas a porta 8502 é uma convenção usada por várias ferramentas como padrão. O padrão é 8502 no -dev mode |
| LAN Serf: The Serf LAN port (TCP and UDP) | 8301 | É usado para lidar com Lan Gossip. Requerido por todos os agentes |
| Wan Serf: The Serf WAN port (TCP and UDP) | 8302 | Isso é usado por servidores para WAN Gossip, para outros servidores |
| server: Server RPC address (TCP Only) | 8300 | É usado pelos servidores para lidar com solicitações recebidas de outros agentes |
| Sidecar Proxy Min: Inclusive min port number to use for automatically assigned sidecar service registrations. | 21000 | NULL |
| Sidecar Proxy Max: Inclusive max port number to use for automatically assigned sidecar service registrations. | 21255 | NULL |

<https://github.com/orgs/hashicorp/repositories?q=consul&type=all&language=&sort=>