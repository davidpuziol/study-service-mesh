# Projeto Hello World
Esse é o exemplo clássico para apresentação do consul.

![Hello World](../../pics/cosul-hello-world.jpg)

O docker compose levantará 3 containers. Em um dos containers a imagem conterá o agent consul rodando em modo server e nos demais, o consul rodando em modo client.

O container rodando em consul em modo server proverá um serviço do nginx em modo proxy e os outros somente fornecerá o nginx simplesmente.

O containers client se juntarão ao container service avisando os seus serviços disponíveis e suas portas.

Logo, toda vez que acessarmos o container service em um dns específico, bateremos em algum dos dois containers client alternadamente e faremos algumas experiências no caminho.

# Instalação


